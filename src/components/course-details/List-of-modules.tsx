import { Button, Box, Heading, List, ListIcon, ListItem, Text, useColorModeValue } from "@chakra-ui/react";
import Link from "next/link";
import React from "react";
import ModuleListWithSLTs from "../lms/Course/ModuleListWithSLTs";

const ListOfModules = () => {
  const textColorBlue = useColorModeValue("theme.darkBlue", "theme.blue")
  return (
    <Box w="95%" marginTop="2em">
      <Heading size="2xl" color={textColorBlue}>
        Course Outline
      </Heading>
      <Text fontSize="xl" pb="5">
        Course modules are numbered. The Student Learning Targets (SLTs) in each Module are also numbered. The purpose of
        this numbering system is to give all of us a quick way to reference what you are learning.
      </Text>
      <Text fontSize="lg" fontWeight="bold" color="theme.yellow" pb="5">
        Click on a Module to view Student Learning Targets.
      </Text>
      <ModuleListWithSLTs />
      <Link href="/get-started/getting-help">
        <Button>How to get help</Button>
      </Link>
    </Box>
  );
};

export default ListOfModules;
